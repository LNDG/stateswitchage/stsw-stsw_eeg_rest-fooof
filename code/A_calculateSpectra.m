% Calculate 1/f spectra for StateSwitch Dynamic

% Note: FOOOF requires linear spectra, so let's accomodate
% We also do not use CSD here, some spatialcorrelation may actually help

%% initialize

restoredefaultpath;
clear all; close all; pack; clc;

%% pathdef

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/';
pn.dataIn       = [pn.root, 'A_preproc/SA_preproc_study/B_data/C_EEG_FT/'];
pn.History      = [pn.root, 'A_preproc/SA_preproc_study/B_data/D_History/'];
pn.tools        = [pn.root, 'B_analyses/S2_TFR/T_tools/']; addpath(pn.tools);
pn.FieldTrip    = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S2_TFR_PeriResponse/T_tools/fieldtrip-20170904/']; addpath(pn.FieldTrip); ft_defaults;

pn.dataOut = [pn.root, 'B_analyses/G_fooof/B_data/']; mkdir(pn.dataOut)

%% define Condition & IDs for preprocessing

cond_names = {'EC'; 'EO'};

%% define IDs for segmentation

% N = 47 YA, 52 OAs
% 2201 - no rest available; 1213 dropped (weird channel arrangement)

IDs = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1138';...
    '1144';'1151';'1158';'1160';'1163';'1164';'1167';'1169';'1172';'1173';...
    '1178';'1182';'1215';'1216';'1219';'1221';'1223';'1227';'1228';...
    '1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';...
    '1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';...
    '2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';...
    '2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';...
    '2149';'2157';'2160';'2202';'2203';'2205';'2206';'2209';'2210';...
    '2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';...
    '2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';...
    '2252';'2258';'2261'};

for id = 1:length(IDs)
        
    display(['processing ID ' IDs{id}]);
    
    for indCond = 1:2

        %% load data

        load([pn.dataIn, IDs{id}, '_rest_EEG_Rlm_Fhl_rdSeg_Art_',cond_names{indCond},'.mat'], 'data');

        % change time indices to all be aligned around 0
        
        for indTrial = 1:numel(data.time)
           data.time{indTrial} = -1.5:1/data.fsample:1.5;
           data.time{indTrial} = data.time{indTrial}(2:end);
        end
        
        %% calculate FFT
        
        param = [];
        param.FFT.trial       = 'all';
        param.FFT.method      = 'mtmconvol';       % analyze entire spectrum; multitaper
        param.FFT.output      = 'pow';          % only get power values
        param.FFT.keeptrials  = 'no';           % return individual trials
        param.FFT.taper       = 'hanning';      % use hanning windows
        param.FFT.tapsmofrq   = 2;
        param.FFT.foi         = 1:1:80;  % frequencies of interest
        param.FFT.t_ftimwin   = repmat(2.5,1,numel(param.FFT.foi));
        param.FFT.toi         = 0;
        param.FFT.pad         = 10;
                
        tmpFreq = ft_freqanalysis(param.FFT, data);
        
%         figure; plot(log10(param.FFT.foi), log10(squeeze(nanmean(tmpFreq.powspctrm(55:60,:),1))))
        
        if indCond == 1
            freq = tmpFreq;
            freq.powspctrm = [];
            freq.powspctrm(1,:,:) = tmpFreq.powspctrm;
        else
            freq.powspctrm(indCond,:,:) = tmpFreq.powspctrm;
        end
        clear tmpFreq;
        
    end

    %% save output
    
    save([pn.dataOut, IDs{id}, '_1f_fooof.mat'], 'freq');
        
end % ID