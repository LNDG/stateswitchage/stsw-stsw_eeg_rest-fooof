
# coding: utf-8

#import sys
#!{sys.executable} -m pip install fooof
import numpy as np
import os as os
from scipy.io import loadmat, savemat
from fooof import FOOOFGroup

IDs = ['1117', '1118', '1120', '1124', '1126', '1131', '1132', '1135', '1136', '1138', 
       '1144', '1151', '1158', '1160', '1163', '1164', '1167', '1169', '1172', '1173', 
       '1178', '1182', '1215', '1216', '1219', '1221', '1223', '1227', '1228', '1233', 
       '1234', '1237', '1239', '1240', '1243', '1245', '1247', '1250', '1252', '1257', 
       '1261', '1265', '1266', '1268', '1270', '1276', '1281', 
       '2104', '2107', '2108', '2112', '2118', '2120', '2121', '2123', '2125', '2129', 
       '2130', '2131', '2132', '2133', '2134', '2135', '2139', '2140', '2145', '2147', 
       '2149', '2157', '2160', '2202', '2203', '2205', '2206', '2209', '2210', 
       '2211', '2213', '2214', '2215', '2216', '2217', '2219', '2222', '2224', '2226', 
       '2227', '2236', '2237', '2238', '2241', '2244', '2246', '2248', '2250', '2251', 
       '2252', '2258', '2261']

IDs = ['2202', '2203', '2205', '2206', '2209', '2210', 
       '2211', '2213', '2214', '2215', '2216', '2217', '2219', '2222', '2224', '2226', 
       '2227', '2236', '2237', '2238', '2241', '2244', '2246', '2248', '2250', '2251', 
       '2252', '2258', '2261']

for curSub in IDs:

    # Set up current paths etc. 
    #curSub = 'CC110033'
    #rootDir = '/Users/kosciessa/Desktop/mntTardis/CamCan/B_rest/'
    print("Setting up paths ...\n")
    rootDir = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/G_fooof/B_data/'
    inPath = rootDir+curSub+'_1f_fooof.mat'
    outPath = rootDir
    if not os.path.isdir(outPath):
        os.mkdir(outPath)
    
    # Load the mat file 
    print("Loading backgrounds from .mat ...\n")
    dat = loadmat(inPath)
    
    # Unpack data from dictionary, and squeeze into numpy arrays
    psds_3d = np.squeeze(dat['freq']['powspctrm'][0][0]).astype('float')
    # choose only the first condition here
    for condition_idx in range(psds_3d.shape[0]): 
        psds = psds_3d[condition_idx,:,:]
        freqs = np.squeeze(dat['freq']['freq'][0][0]).astype('float')
        # ^Note: this also explicitly enforces type as float (type casts to float64, instead of float32)
        #  This is not strictly necessary for fitting, but is for saving out as json from FOOOF, if you want to do that
        # FOOOF expects psds to be nChan*nFreq, no preference for 1D freqs
    
        # Initialize FOOOF object
        fg = FOOOFGroup()
        
        # Fit the FOOOF model on all PSDs, and report
        fg.report(freqs, psds, [2, 80])
        
        # for channel in range(len(fg.get_params('error'))):
        #     fm = fg.get_fooof(channel, regenerate=True)
        #     fm.plot()
        
        # Save out a specific FOOOF measure of interest - for example, slopes
        exps = fg.get_params('aperiodic_params', 'exponent')
        savemat(rootDir+'exps'+'_'+curSub+'_'+str(condition_idx+1)+'.mat', {'exps' : exps})
        
        # Save out fooof results to json file
        #  There is a utility file to load this json file directly into Matlab
        fg.save(rootDir+'fooof_results'+'_'+curSub+'_'+str(condition_idx+1), save_results=True)
    
    print(curSub+'Done!')